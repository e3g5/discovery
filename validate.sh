#!/bin/bash

validate() {
    export TASK_ID=$(AWS_PROFILE=terraform aws --region=us-west-2 ecs list-tasks --cluster discovery-ecs-cluster | jq '.taskArns[0]' | tr -d "\"")
    export ENI=$(AWS_PROFILE=terraform aws --region=us-west-2 ecs describe-tasks --cluster discovery-ecs-cluster --tasks $TASK_ID | jq '.tasks[0].attachments[].details[1].value' | tr -d "\"")
    export PUBLIC_IP=$(AWS_PROFILE=terraform aws --region=us-west-2 ec2 describe-network-interfaces --network-interface-ids $ENI | jq '.NetworkInterfaces[0].Association.PublicIp' | tr -d "\"")
    echo $PUBLIC_IP
    curl http://$PUBLIC_IP:8080
}

validate
