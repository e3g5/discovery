import configparser
from dataclasses import dataclass
import os
from time import sleep
from typing import Any, Dict
import boto3


@dataclass
class IAMBootstrap:
    """
    Bootstrap an AWS account with a Terraform IAM user
    """

    account_id: str = ""
    client: boto3.client = None
    credentials: Dict[str, Any] = None
    profile_name: str = "terraform"
    role_name: str = "terraform"
    username: str = "terraform"

    def get_iam_client(self):
        self.client = boto3.client('iam')

    def get_account_id(self):
        self.account_id = boto3.client('sts').get_caller_identity().get('Account')

    def create_terraform_user(self):
        paginator = self.client.get_paginator('list_users')
        for page in paginator.paginate():
            if "terraform" not in page.get('Users', {}):
                self.client.create_user(
                    UserName=self.username
                )
                # Grant AdministratorAccess to the user
                self.client.attach_user_policy(
                    UserName=self.username,
                    PolicyArn='arn:aws:iam::aws:policy/AdministratorAccess'
                )


    def create_terraform_user_access_key(self):
        self.credentials = self.client.create_access_key(
            UserName=self.username
        )

    def create_terraform_profile(self):
        config_file = os.path.expanduser("~/.aws/config")
        config = configparser.ConfigParser()
        if os.path.exists(config_file):
            config.read(config_file)
        if not config.has_section(self.profile_name):
            config.add_section(self.profile_name)
        config.set(self.profile_name, 'region', 'us-west-2')
        with open(config_file, 'w') as file:
            config.write(file)

    def write_terraform_user_credentials(self):
        credentials_file = os.path.expanduser("~/.aws/credentials")
        config = configparser.ConfigParser()
        if os.path.exists(credentials_file):
            config.read(credentials_file)

        if not config.has_section(self.profile_name):
            config.add_section(self.profile_name)
        config.set(self.profile_name, 'aws_access_key_id', self.credentials.get("AccessKey", {}).get("AccessKeyId", ""))
        config.set(self.profile_name, 'aws_secret_access_key',
                   self.credentials.get("AccessKey", {}).get("SecretAccessKey", ""))

        with open(credentials_file, 'w') as file:
            config.write(file)

    def run(self):
        self.get_iam_client()
        self.get_account_id()
        print(f"Creating {self.username} user...")
        self.create_terraform_user()
        print(f"Creating {self.username} user credentials...")
        self.create_terraform_user_access_key()
        print(f"Creating {self.profile_name} profile...")
        self.create_terraform_profile()
        print(f"Writing {self.profile_name} credentials...")
        self.write_terraform_user_credentials()


def iam_bootstrap():
    return IAMBootstrap().run()


if __name__ == "__main__":
    iam_bootstrap()
