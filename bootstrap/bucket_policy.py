import json


policy_string = json.dumps({
"Version": "2012-10-17",
"Statement": [{
    "Sid": "TerraformStateBucketPolicy",
    "Effect": "Allow",
    "Principal": "*",
    "Action": [
        "s3:PutObject*",
        "s3:List*",
        "s3:Get*",
        "s3:Delete*"
    ],
    "Resource": [
        "arn:aws:s3:::{bucket_name}",
        "arn:aws:s3:::{bucket_name}/*"
    ],
}]
})
