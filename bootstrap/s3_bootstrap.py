from dataclasses import dataclass
from time import sleep
import uuid
from bucket_policy import policy_string

import boto3 

@dataclass
class S3Bootstrap:
    bucket_name: str = f"terraform-us-west-2-{uuid.uuid4().hex[:10]}"
    client: boto3.client = None


    def get_client(self):
        self.client = boto3.client("s3")
    
    def create_bucket(self):
        buckets = self.client.list_buckets()
        if "terraform-state-us-west-2" not in buckets.get("Buckets", []):
            print(f"Creating bucket {self.bucket_name}...")
            bucket_policy = policy_string.replace("{bucket_name}", self.bucket_name)
            self.client.create_bucket(
                Bucket=self.bucket_name,
                CreateBucketConfiguration={
                    'LocationConstraint': 'us-west-2'
                },
            )
            print("Waiting for bucket to be created...")
            sleep(30)
            print("Removing public access block for policy application...")
            self.client.delete_public_access_block(
                Bucket=self.bucket_name,
            )
            print("Applying bucket policy...")
            self.client.put_bucket_policy(Bucket=self.bucket_name, Policy=bucket_policy)
            sleep(10)
            print("Applying public access block...")
            self.client.put_public_access_block(
                Bucket=self.bucket_name,
                PublicAccessBlockConfiguration={
                    'BlockPublicAcls': True,
                    'IgnorePublicAcls': True,
                    'BlockPublicPolicy': True,
                    'RestrictPublicBuckets': True
                },
            )
    
    def run(self):
        self.get_client()
        self.create_bucket()

def s3_bootstrap():
    return S3Bootstrap().run()

if __name__ == "__main__":
    s3_bootstrap()
