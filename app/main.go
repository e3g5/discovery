package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", message).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func message(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, Discovery team!")
}
