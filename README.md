# Discovery

This repository contains the documentation and code to deploy a conatinerized application to AWS with all of the requisite infrastructure and configuration.

## Bootstrapping

### venv 

`boto3` is used for the bootstrapping portion of this assessment. To get started, run:
    - `python3 -m venv venv`
    - `source venv/bin/activate` or `source venv/bin/activate.fish` if using Fish
    - `pip3 install -r bootstrap/requirements.txt`

### Temporary Access Key

To get started with the pre-requisites for the Terraform deployment, follow these steps for generating a temporary access key:
- Using an existing IAM user with IAM creation permissions, generate a new access key under `IAM -> Users -> <User> -> Create access key`
- Select `Command Line Interface (CLI)` for the `Use case`
- Check the `I understand the above recommendation and want to proceed to create an access key.` since SSO is not configured for use with the AWS CLI
- Click `Next`
- Set the description tag to `Discovery Assessment`
- Export the new keys as environment variables:
```sh
export AWS_ACCESS_KEY_ID=<Access key>; export AWS_SECRET_ACCESS_KEY=<Secret access key>
```

The AWS CLI is installed when initializing the `venv`, so once the access keys are exported, run `aws sts get-caller-identity` to ensure that authentication to AWS is working:
```sh
$ aws sts get-caller-identity
{
    "UserId": "AIDAS75WS3IMHZCZAOSNF",
    "Account": "123456789012",
    "Arn": "arn:aws:iam::123456789012:user/user_name"
}
```

Once authenticated, the bootstrapping process can begin, started with the IAM resources and then the state bucket (DynamoDB lock table and S3 bucket).

After running `bootstrap/iam_bootstrap.py`, `unset` the exisiting `AWS_` credentials and run:
```sh
$ AWS_PROFILE=terraform aws sts get-caller-identity
{
    "UserId": "AIDAS75WS3IMKNXLGC2YV",
    "Account": "123456789012",
    "Arn": "arn:aws:iam::123456789012:user/terraform"
}
```

This will confirm that the `terraform` role is being used.

### Terraform IAM User

A separate `terraform` user will be used to assume a Terraform IAM role to keep access locked down. This user will be created along with the IAM role with the IAM bootstrap script.

Rather than using a random IAM user with `AdministratorAccess`, this code uses a static IAM user with access to perform all of the necessary AWS operations. To facilitate this, an IAM bootstrapping script is included to create the IAM user which can then be used for the following operations:
1. State Bucket bootstrapping
2. `terraform apply`
3. Docker image pushes

After running `bootstrap/iam_bootstrap.py`, `unset` the exisiting `AWS_` credentials and run:
```sh
$ AWS_PROFILE=terraform aws sts get-caller-identity
{
    "UserId": "AIDAS75WS3IMKNXLGC2YV",
    "Account": "123456789012",
    "Arn": "arn:aws:iam::123456789012:user/terraform"
}
```

This will confirm that the `terraform` user is being used. It is important to always prefix both `aws` and `terraform` commands with `AWS_PROFILE=terraform` to ensure that the appropriate level of CRUD access for resources remains in place.

### State Bucket

In order to deploy infrastructure and maintain state, a Terraform state bucket is needed. To avoid a chicken-and-egg scenario, creating the state bucket out-of-band is easiest. This can be done via the AWS Console or via a bootstrapping script. For the purposes of this assessment, the latter is also included to avoid needing manual console usage.

The state bucket is created by running the `bootstrap/s3_bootstrap` script. The state bucket also contains a bucket policy to all for the creation and updating of the Terraform state file.

KMS encryption for the bucket, while a best practice, is out of scope for this effort.

Once the state bucket is created, the Terraform deployment process can begin.

## Terraform

The Terraform code is split up into three distinct modules:
- A DynamoDB module to handle the lock table for the Terraform state
- An ECR module to create an ECR repository to host the container image
- An ECS module to create an ECS cluster, a Task Definition, a Service, and ancillary resources like the execution role and security group
    - This module includes data sources for retrieving the default VPC and Subnets
        - Creating separate network resources was out of scope for this effort but ideally this would be done in a production setting

Most of the Terraform variables use defaults with exceptions which are overridden by the module callers in the base `main.tf` file. Additionally, outputs are provided for easy reference. This is especially important when tagging the Docker image and pushing it.

To deploy the Terraform code, begin with `backend.tf` commented out as well as the non-DynamoDB module callers. This will initialize a local state file and create the DynamoDB lock table.

To apply this configuration, run these commands:
    - `make init`
    - `make plan`
    - `make apply`

Once the DynamoDB lock table is created, uncomment the contents of `backend.tf` and the ECR and ECS module callers. Once that is done, run `make init` and copy the state over to S3.

Since the Docker image will not be ready yet, use a placeholder image for the `image` input in `module.ecs_cluster`. Something like `httpd` which is publicly available should work.

After the image is configured, run a `make plan` and `make apply` to create the other resources. 

## Application

### Code

The web app code is a straightforward HTTP app that serves content to the `/` route over port `8080`:
```go
func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", message).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func message(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, Discovery team!")
}
```

When running `curl` to the public IP for the Fargate Task, the following result is returned:
```
Hello, Discovery team!
```

To validate this behavior after running `terraform apply`, run `make validate` which retrieves the Task ID, its ENI, and the public IP associated with the ENI.

### Image

The Docker image uses a multi-stage build to build the Go app and then serve the app over an exposed port (`8080`).

```Dockerfile
# Build stage
FROM golang:1.20-bookworm AS builder
WORKDIR /
COPY go.* *.go ./
RUN go mod download
RUN CGO_ENABLED=0 go build -o ./discovery

# Serve stage
FROM gcr.io/distroless/base-debian11
WORKDIR /
COPY --from=builder /discovery /discovery
EXPOSE 8080
ENTRYPOINT ["/discovery"]
```

Size and efficiency:
```sh
Image name: 123456789012.dkr.ecr.us-west-2.amazonaws.com/discovery:latest
Total Image size: 24 MB
Potential wasted space: 613 B
Image efficiency score: 99 %
```

CVEs:
```sh
❯ grype 123456789012.dkr.ecr.us-west-2.amazonaws.com/discovery:latest                                                                                                     (discovery/app) 07:08:07
 ✔ Vulnerability DB                [no update available]
 ✔ Loaded image                                                                                                                     discovery:latest
 ✔ Parsed image                                                              sha256:1dddfb0d1c43905bce2625c22e346b7896672bdefc347152156abab167555bbe
 ✔ Cataloged packages              [4 packages]
 ✔ Scanned for vulnerabilities     [0 vulnerabilities]
   ├── 0 critical, 0 high, 0 medium, 0 low, 0 negligible
   └── 0 fixed
[0000]  WARN some package(s) are missing CPEs. This may result in missing vulnerabilities. You may autogenerate these using: --add-cpes-if-none
No vulnerabilities found
```

To build the image, run `make build`. This will result in a local `discovery:latest` image being built that can then be tagged for pushing to the newly-created ECR repository.

To do so, retrieve the `ecr_repository_url` from the Terraform outputs (or run `make apply` again if that output is gone) and run a `docker tag discovery:latest <URL>:latest`, so for example:
```sh
docker tag discovery:latest 123456789012.dkr.ecr.us-west-2.amazonaws.com/discovery:latest
```

Before pushing the image to ECR, authenticate to the ECR repository. This can be done by running:
```
AWS_PROFILE=terraform aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin <Account ID>.dkr.ecr.us-west-2.amazonaws.com
```

Once authenticated, run a `docker push` -- for example:
```sh
docker push 123456789012.dkr.ecr.us-west-2.amazonaws.com/discovery:latest
```

Once the image is in the ECR repository, update the `image` input in the `ecs_cluster` module to the new image URL:
```hcl
image = 123456789012.dkr.ecr.us-west-2.amazonaws.com/discovery:latest
```

and run a final `make apply`

This will update the Task Definition to use the new image and an the existing Task may need to be stopped either in the Console or via the CLI.

Once the new Task is running on the correct image, run the validation script (`validate.sh`) by running
`make validate` (locating the Public IP in the Console and visiting it at `http://<Public IP>:8080` also works).

## Troubleshooting

### Bootstrapping

Re-running the bootstrapping scrips will sometimes result in failures if resources alreadty exist. Manually cleaning these up via the Console is easiest whether it be the IAM user or the state bucket. Once the resources are cleaned up (if necessary), the bootstrapping sscripts can be run.

The bootstrapping scripts also hardcode sleep downtimes due to AWS' eventual consistency.

### Terraform

The most likely issue with the Terraform code is network reachability to the Fargate container. While this example is straightforward, mis-configuring the CIDR blocks or ingress ports on the security group can block an otherwise functioning example, so be mindful of this configuration.

For this exercise, all ports should remain set to `8080` both in the app code and in the Terraform code.

### Docker

The Docker image build must use `CGO_ENABLED=0` in the build stage or `glibc` errors will occur when attempting to run the app.

## Tradeoffs and Future Considerations

From a compliance standpoint, this is not the _best_ example mostly due to the lack of KMS encryption of the state bucket, no [encrypted] CloudWatch logs, etc. Were this to be in a FedRAMP environment, more work would need to be done to conform to the operational controls for FedRAMP Moderate/High.

The bootstrapping portion could have easily been a Bash Script, Cloudformation, or even Golang, but `boto3` is easy so that was chosen rather than the other options. In the future, a more standardized bootstrapping process would be necessary since this is always a point of contention when getting started with non-HCP Terraform.

The app code could have also been hard-coded Bash in the Task Definition or a small Flask app, but I opted for Golang since it's small and lightweight. I also wanted to ensure that I had control over the container image.

The ECS module was more consolidated than it otherwise could be; for example, an IAM module to create the task execution role and a network module to create the security group would have been a more production-ready layout. That said, these resources directly impact their neighboring resources in the module so I opted to group them together.

As far as the Terraform code as a whole is concerned, hosting the modules in a versioned repository would go a long way in avoiding breaking changes. 

On the networking side, using an ALB that forwards traffic to the Task would be nice as well. Any time a Task is stopped and started, a new Public IP is used which is not great for production use.

## Cleanup

To clean up all of the resources from this example, run a `terraform destory` to remove the DynamoDB, ECR, and ECS, resources.

Once the Terraform resources are detroyed, manually delete the State Bucket and `terraform` user from the Console.
