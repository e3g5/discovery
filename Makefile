.PHONY: build tag account-id ecr-auth push fmt lint init plan apply bootstrap_iam bootstrap_s3

build:
	docker build --platform linux/arm64 -t discovery:latest ./app

account-id:
	AWS_PROFILE=terraform aws sts get-caller-identity --query Account --output text

ecr-auth:
	AWS_PROFILE=terraform aws ecr get-login-password --region us-west-2 | docker login --username AWS --password-stdin $(ARGS)

fmt:
	terraform fmt -recursive

lint:
	terraform validate

init:
	AWS_PROFILE=terraform terraform -chdir=./terraform init 

plan:
	AWS_PROFILE=terraform terraform -chdir=./terraform plan

apply:
	AWS_PROFILE=terraform terraform -chdir=./terraform apply --auto-approve

validate:
	./validate.sh

bootstrap_iam:
	python3 ./bootstrap/bootstrap_iam.py

bootstrap_s3:
	AWS_PROFILE=terraform python3 ./bootstrap/bootstrap_s3.py
