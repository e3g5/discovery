module "lock_table" {
  source = "./modules/dynamodb"
  name   = "terraform-lock-table"
}

module "ecr_repo" {
  source = "./modules/ecr"
  name   = "discovery"
}

module "ecs_cluster" {
  source         = "./modules/ecs"
  name           = "discovery-ecs-cluster"
  service_name   = "discovery_service"
  container_name = "discovery"
  container_port = 8080
  host_port      = 8080
  # replace the image with your own ECR repo
  image = "206004476440.dkr.ecr.us-west-2.amazonaws.com/discovery:latest"
}
