output "lock_table_name" {
  value = module.lock_table.lock_table_name
}

output "lock_table_arn" {
  value = module.lock_table.lock_table_arn
}

output "ecr_repository_url" {
  value = module.ecr_repo.ecr_repository_url
}

output "ecs_cluster_arn" {
  value = module.ecs_cluster.ecs_cluster_arn
}

output "ecs_task_definition_arn" {
  value = module.ecs_cluster.ecs_task_definition_arn
}

output "ecs_service_id" {
  value = module.ecs_cluster.ecs_service_id
}
