provider "aws" {
  region = "us-west-2"
}

# Add this after creating the lock table with local state
# Once created, run a terraform init with this block and migrate the state to S3
terraform {
  required_version = "= 1.5.5"
  backend "s3" {
    bucket         = "terraform-us-west-2-637fdedda7" # run AWS_PROFILE=terraform aws s3 ls to get this
    key            = "discovery/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "terraform-lock-table-us-west-2"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.13.1"
    }
  }
}
