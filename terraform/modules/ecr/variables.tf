variable "name" {
  type    = string
  default = ""
}

variable "force_delete" {
  type    = bool
  default = true
}

variable "image_tag_mutability" {
  type    = string
  default = "MUTABLE"
}

variable "image_scanning_configuration" {
  type = list(object({
    scan_on_push = bool
  }))
  default = [
    {
      scan_on_push = true
    }
  ]
}
