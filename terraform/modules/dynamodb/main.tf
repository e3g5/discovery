resource "aws_dynamodb_table" "terraform_lock" {
  name         = format("%s-%s", var.name, var.region)
  billing_mode = var.billing_mode
  hash_key     = var.hash_key
  dynamic "attribute" {
    for_each = var.attributes
    content {
      name = attribute.value.name
      type = attribute.value.type
    }
  }
}
