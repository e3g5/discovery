variable "region" {
  type    = string
  default = "us-west-2"
}

variable "name" {
  type    = string
  default = ""
}

variable "billing_mode" {
  type    = string
  default = "PAY_PER_REQUEST"
}

variable "hash_key" {
  type    = string
  default = "LockID"
}

variable "attributes" {
  type = list(object({
    name = string
    type = string
  }))
  default = [
    {
      name = "LockID"
      type = "S"
    }
  ]
}
