variable "name" {
  type    = string
  default = ""
}

variable "settings" {
  type = list(object({
    name  = string
    value = string
  }))
  default = [{
    name  = "containerInsights"
    value = "enabled"
  }]
}

variable "service_name" {
  type    = string
  default = ""
}

variable "family" {
  type    = string
  default = "service"
}

variable "requires_compatibilities" {
  type    = list(string)
  default = ["FARGATE"]
}

variable "container_name" {
  type    = string
  default = ""
}

variable "image" {
  type    = string
  default = "public.ecr.aws/docker/library/httpd:latest"
}

variable "cpu" {
  type    = number
  default = 256
}

variable "memory" {
  type    = number
  default = 512
}

variable "essential" {
  type    = bool
  default = true
}

variable "container_port" {
  type    = number
  default = 80
}

variable "host_port" {
  type    = number
  default = 80
}

variable "placement_constraints" {
  type = list(object({
    type       = string
    expression = string
  }))
  default = [
    {
      type       = "memberOf"
      expression = "attribute:ecs.availability-zone in [us-west-2a, us-west-2b, us-west-2c, us-west-2d]"
    }
  ]
}

variable "network_mode" {
  type    = string
  default = "awsvpc"
}

variable "assign_public_ip" {
  type    = bool
  default = true
}

variable "security_group_name" {
  type    = string
  default = "discovery-app-sg"
}

variable "security_group_description" {
  type    = string
  default = "Security group for the discovery web app"
}

variable "ingress_rules" {
  type = list(object({
    from_port = number
    to_port   = number
    protocol  = string
  }))
  default = [
    {
      from_port = 8080
      to_port   = 8080
      protocol  = "tcp"
    }
  ]
}
