output "ecs_cluster_arn" {
  value = aws_ecs_cluster.discovery.arn
}

output "ecs_task_definition_arn" {
  value = aws_ecs_task_definition.discovery_service.arn
}

output "ecs_service_id" {
  value = aws_ecs_service.discovery.id
}
